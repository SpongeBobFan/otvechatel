import logging

from aiohttp import web

log = logging.getLogger(__name__)


class ResponseServer:
    def __init__(self, config):
        self.config = config
        self._app = web.Application()

    def configure_endpoints(self):
        self._app.add_routes([web.get('/', self.handle_web),
                              web.route('*', '/target', self.handle_target),
                              web.route('*', '/echo', self.handle_echo)])

    def run(self):
        port = self.config['common']['port']
        web.run_app(self._app, port=port)

    async def handle_web(self, request):
        filename = 'index.html'
        file = open(filename, 'r')
        page = file.read()
        log.debug('responding with web page from file: ' + filename)
        return web.Response(text=page, headers=[('Content-Type', 'text/html')])

    async def handle_target(self, request):
        need_to_respond = self.config.getboolean('target', 'need_to_respond')
        if need_to_respond:
            filename = self.config['target']['response_file']
            file = open(filename, 'r')
            msg = file.read()
            headers = self._get_headers()
            log.debug('responding with file: ' + filename)
            log.debug('and headers:' + str(headers))
            return web.Response(text=msg, headers=headers)
        else:
            log.debug('sending empty response')
            return web.Response(headers=[('Content-Type', 'text/plain')])

    async def handle_echo(self, request):
        msg_bytes = await request.read()
        msg = msg_bytes.decode('utf-8')
        hdrs = request.headers;
        print(msg)
        print(hdrs)
        log.debug('echo (with default header)')
        #return web.Response(text=msg, headers=hdrs)
        return web.Response(text=msg, headers=[('Content-Type', 'text/plain')])

    def _get_headers(self):
        res = []
        try:
            hdrs_plain = self.config['target']['response_headers']
        except KeyError:
            log.debug('response_headers is not set. Using default values')
            return [('Content-Type', 'text/plain')]
        hdrs_lines = hdrs_plain.split('\n')
        for line in hdrs_lines:
            hdrs_kv = line.split(' ')
            res.append((hdrs_kv[0], hdrs_kv[1]))
        return res
