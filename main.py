import configparser
import logging

from response_server import ResponseServer


def main():
    config = configparser.ConfigParser()
    config.read('config.ini')

    logging.basicConfig(level=logging.DEBUG)

    srv = ResponseServer(config)
    srv.configure_endpoints()
    srv.run()


if __name__ == '__main__':
    main()
